package quickcheck

import common._

import org.scalacheck._
import Arbitrary._
import Gen._
import Prop._
import scala.util.Random

abstract class QuickCheckHeap extends Properties("Heap") with IntHeap {

  property("find.insert.empty") = forAll { a: A =>
    val h = insert(a, empty)
    findMin(h) == a
  }

  property("find.inserts.empty") = forAll { (a: A, b: A) =>
    val h = insert(a, insert(b, empty))
    min(a, b) == findMin(h)
  }

  property("find.insert.any") = forAll { (h: H) =>
    val a = if (isEmpty(h)) 0 else findMin(h)
    findMin(insert(a, h)) == a
  }

  property("prioritized.any") = forAll { (h: H) =>
    isPrioritized(findMin(h), h)
  }

  property("prioritized.meld.anys") = forAll { (g: H, h: H) =>
    min(findMin(g), findMin(h)) == findMin(meld(g, h))
  }

  property("insert.twice") = forAll { (a: A, b: A) => (a < b) ==>  (
    a == findMin(deleteMin(insert(a, insert(b, insert(a, empty))))) )
  }

  property("equal.any") = forAll { (h: H) =>
    equalHeaps(h, h)
  }

  property("equal.meld.insert.anys") = forAll { (g: H, h: H) => (!(isEmpty(g) || isEmpty(h))) ==> ( {
    val mG = findMin(g)
    equalHeaps(meld(g, h), meld(deleteMin(g), insert(mG, h))) } )
  }

  def equalHeaps(g: H, h: H): Boolean = {
    if (isEmpty(g) && isEmpty(h)) true
    else {
      if (isEmpty(g) || isEmpty(h)) false
      else (findMin(g) == findMin(h)) && equalHeaps(deleteMin(g), deleteMin(h))
    }
  }

  def isPrioritized(m: A, h: H): Boolean =
    if (isEmpty(h)) true
    else {
      val c = findMin(h)
      (m <= c) && isPrioritized(c, deleteMin(h))
    }

  def min(a: Int, b: Int): Int = if (a < b) a else b

  lazy val genHeap: Gen[H] = for {
    x <- arbitrary[Int]
    h <- oneOf(value(empty), genHeap)
  } yield insert(x, h)

  implicit lazy val arbHeap: Arbitrary[H] = Arbitrary(genHeap)
}
