package kvstore

import akka.actor.{Cancellable, Props, Actor, ActorRef}
import scala.concurrent.duration._
import scala.language.postfixOps

object Replicator {
  case class Replicate(key: String, valueOption: Option[String], id: Long)
  case class Replicated(key: String, id: Long)
  
  case class Snapshot(key: String, valueOption: Option[String], seq: Long)
  case class SnapshotAck(key: String, seq: Long)

  def props(replica: ActorRef): Props = Props(classOf[Replicator], replica)
}

class Replicator(val replica: ActorRef) extends Actor {
  import Replicator._
  import context.dispatcher

  /*
   * The contents of this actor is just a suggestion, you can implement it in any way you like.
   */

  case class SnapshotTimedOut(seq: Long)

  // map from sequence number to pair of sender and request
  var acks = Map.empty[Long, (ActorRef, Replicate)]
  var cancels = Map.empty[Long, Cancellable]

  var _seqCounter = 0L
  def nextSeq = {
    val ret = _seqCounter
    _seqCounter += 1
    ret
  }

  def receive: Receive = {
    case Replicate(key, valueOption, id) =>
      val seq = nextSeq
      acks = acks + (seq -> (sender, Replicate(key, valueOption, id)))
      cancels = cancels + (seq -> context.system.scheduler.schedule(
        0 milliseconds,
        100 milliseconds,
        replica,
        Snapshot(key, valueOption, seq)))
      context.system.scheduler.scheduleOnce(1 second, self, SnapshotTimedOut(seq))

    case SnapshotAck(key, seq) =>
      if (cancels.isDefinedAt(seq)) {
        cancels(seq).cancel()
        cancels = cancels - seq
        acks(seq) match {
          case (requester, request) => requester ! Replicated(key, request.id)
        }
        acks = acks - seq
      } else {
        // we could receive Persisted after ReplicateTimedOut
      }

    case SnapshotTimedOut(seq) =>
      if (cancels.isDefinedAt(seq)) {
        cancels(seq).cancel()
        cancels = cancels - seq
        if (acks.isDefinedAt(seq)) {
          acks = acks - seq
        }
      }
  }
}
