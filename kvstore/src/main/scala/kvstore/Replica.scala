package kvstore

import akka.actor._
import kvstore.Arbiter._
import scala.collection.immutable.Queue
import akka.actor.SupervisorStrategy.{Escalate, Restart}
import scala.annotation.tailrec
import akka.pattern.{ ask, pipe }
import scala.concurrent.duration._
import akka.util.Timeout
import scala.util.Random
import scala.Some
import akka.actor.OneForOneStrategy
import kvstore.Arbiter.Replicas
import akka.actor.Terminated
import scala.language.postfixOps

object Replica {
  sealed trait Operation {
    def key: String
    def id: Long
  }
  case class Insert(key: String, value: String, id: Long) extends Operation
  case class Remove(key: String, id: Long) extends Operation
  case class Get(key: String, id: Long) extends Operation

  sealed trait OperationReply
  case class OperationAck(id: Long) extends OperationReply
  case class OperationFailed(id: Long) extends OperationReply
  case class GetResult(key: String, valueOption: Option[String], id: Long) extends OperationReply

  def props(arbiter: ActorRef, persistenceProps: Props): Props = Props(classOf[Replica], arbiter, persistenceProps)
}

class Replica(val arbiter: ActorRef, persistenceProps: Props) extends Actor {
  import Replica._
  import Replicator._
  import Persistence._
  import context.dispatcher

  /*
   * The contents of this actor is just a suggestion, you can implement it in any way you like.
   */
  case class PersistTimedOut(id: Long)

  case class ReplicateTimedOut(id: Long)

  var kv = Map.empty[String, String]
  // a map from secondary replicas to replicators
  var secondaries = Map.empty[ActorRef, ActorRef]
  // the current set of replicators
  var replicators = Set.empty[ActorRef]
  // expected sequence number from replicator
  var expectedSeq = Map.empty[ActorRef, Long].withDefaultValue(0L)

  var persistence = context.actorOf(persistenceProps)

  // map from identifier to pair of sender and request
  var acks = Map.empty[Long, (ActorRef, Persist)]
  // cancellable persist retries
  var cancels = Map.empty[Long, Cancellable]
  // number of replicators per op id we are waiting on
  var waits = Map.empty[Long, Set[ActorRef]]

  arbiter ! Join

  def receive = {
    case JoinedPrimary   => context.become(leader)
    case JoinedSecondary => context.become(replica)
  }

  /* TODO Behavior for the leader role. */
  val leader: Receive = {
    case Insert(key, value, id) =>
      kv = kv + (key -> value)
      persist(key, Some(value), id)
      replicate(key, Some(value), id)

    case Remove(key, id) =>
      kv = kv - key
      persist(key, None, id)
      replicate(key, None, id)

    case Get(key, id)  =>
      sender ! GetResult(key, if (kv.isDefinedAt(key)) Some(kv(key)) else None, id)

    case Replicas(replicas) =>
      val newReplicas = replicas.filter(_ != self) &~ secondaries.keySet
      newReplicas.foreach { join }
      val oldReplicas = secondaries.keySet &~ replicas.filter(_ != self)
      oldReplicas.foreach { leave }

    case Persisted(key, id) =>
      if (cancels.isDefinedAt(id)) {
        cancels(id).cancel()
        cancels = cancels - id
        if (waits(id).isEmpty) {
          waits = waits - id
          acks(id)._1 ! OperationAck(id)
          acks = acks - id
        }
      } else {
        // we could receive Persisted after ReplicateTimedOut
      }

    case Replicated(key, id) =>
      if (waits.isDefinedAt(id)) {
        val curWaits = waits(id)
        waits = waits.updated(id, curWaits - sender)
        if (waits(id).isEmpty && !cancels.isDefinedAt(id)) {
          waits = waits - id
          acks(id)._1 ! OperationAck(id)
          acks = acks - id
        }
      } else {
        // we could receive a Replicated after a PersistTimedOut (cleans everything)
        // so we ignore it, we have already cleaned up everything
      }

    case PersistTimedOut(id) =>
      if (cancels.isDefinedAt(id)) {
        cancels(id).cancel()
        cancels = cancels - id
        if (acks.isDefinedAt(id)) {
          acks(id)._1 ! OperationFailed(id)
          acks = acks - id
        }
      }

    case ReplicateTimedOut(id) =>
      if (acks.isDefinedAt(id)) {
        acks(id)._1 ! OperationFailed(id)
        acks = acks - id
      }
      waits = waits - id
  }

  def persist(key: String, value: Option[String], id: Long): Unit = {
    acks = acks + (id -> (sender, Persist(key, value, id)))
    cancels = cancels + (id -> context.system.scheduler.schedule(
      0 milliseconds,
      100 milliseconds,
      persistence,
      Persist(key, value, id)
    ))

    context.system.scheduler.scheduleOnce(1 second, self, PersistTimedOut(id))
  }

  def replicate(key: String, value: Option[String], id: Long): Unit = {
    waits = waits + (id -> replicators)
    replicators.foreach { r => r ! Replicate(key, value, id)}
    context.system.scheduler.scheduleOnce(1 second, self, ReplicateTimedOut(id))
  }
  
  def join(replica: ActorRef): Unit = {
    val replicator = context.actorOf(Replicator.props(replica))
    secondaries = secondaries + (replica -> replicator)
    replicators = replicators + replicator
    kv.foreach { case (key, value) => replicator ! Replicate(key, Some(value), Random.nextLong())}
  }

  def leave(replica: ActorRef): Unit = {
    val replicator = secondaries(replica)
    replicator ! PoisonPill
    replicators = replicators - replicator
    secondaries = secondaries - replica
    val updatedWaits = for((id, reps) <- waits) yield (id, reps - replicator)
    val acksToWaive = acks.filter { case (id, (requester, _)) => updatedWaits(id).isEmpty }
    acksToWaive.foreach { case (id, (requester, _)) =>
      requester ! OperationAck(id)
    }
    acks = acks -- acksToWaive.keys
    waits = waits ++ updatedWaits
  }

  val replica: Receive = {
    case Get(key, id)  =>
      sender ! GetResult(key, if (kv.isDefinedAt(key)) Option(kv(key)) else None, id)

    case Snapshot(key, valueOption, seq) =>
      if (seq == expectedSeq(sender)) {
        kv = if (valueOption.isDefined) kv + (key -> valueOption.get) else kv - key
        persist(key, valueOption, seq)
        expectedSeq = expectedSeq.updated(sender, seq + 1)
      } else if (seq < expectedSeq(sender)) {
        sender ! SnapshotAck(key, seq)
      } else {
        // seq in the future ... WTF!
      }

    case Persisted(key, seq) =>
      if (cancels.isDefinedAt(seq)) {
        cancels(seq).cancel()
        cancels = cancels - seq
        acks(seq)._1 ! SnapshotAck(key, seq)
        acks = acks - seq
      } else {
        // we could receive Persisted after PersistTimedOut
      }

    case PersistTimedOut(seq) =>
      if (cancels.isDefinedAt(seq)) {
        cancels(seq).cancel()
        cancels = cancels - seq
        acks = acks - seq
      }
  }
}
