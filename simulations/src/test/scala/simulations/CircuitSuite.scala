package simulations

import org.scalatest.FunSuite

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

import org.scalatest.prop.Checkers
import org.scalacheck.Arbitrary._
import org.scalacheck.Prop
import org.scalacheck.Prop._

@RunWith(classOf[JUnitRunner])
class CircuitSuite extends CircuitSimulator with FunSuite with Checkers {
  val NoOpDelay = 0
  val InverterDelay = 1
  val AndGateDelay = 3
  val OrGateDelay = 5
  
  test("andGate example") {
    val in1, in2, out = new Wire
    andGate(in1, in2, out)
    in1.setSignal(false)
    in2.setSignal(false)
    run
    
    assert(out.getSignal === false, "and 1")

    in1.setSignal(true)
    run
    
    assert(out.getSignal === false, "and 2")

    in2.setSignal(true)
    run
    
    assert(out.getSignal === true, "and 3")
  }

  test("orGate") {
    val in1, in2, out = new Wire
    orGate(in1, in2, out)

    in1.setSignal(false)
    in2.setSignal(false)
    run
    assert(out.getSignal === false, "or 0-0")

    in1.setSignal(true)
    run
    assert(out.getSignal === true, "or 1-0")

    in2.setSignal(true)
    run
    assert(out.getSignal === true, "or 1-1")
  }

  test("orGate2") {
    val in1, in2, out = new Wire
    orGate2(in1, in2, out)

    in1.setSignal(false)
    in2.setSignal(false)
    run
    assert(out.getSignal === false, "or 0-0")

    in1.setSignal(true)
    run
    assert(out.getSignal === true, "or 1-0")

    in2.setSignal(true)
    run
    assert(out.getSignal === true, "or 1-1")
  }

  test("noOpGate") {
    val in, out = new Wire
    noOpGate(in, out)

    in.setSignal(false)
    run
    assert(out.getSignal == false, "noOp 0")

    in.setSignal(true)
    run
    assert(out.getSignal === true, "noOp 1")
  }

  test("demux - 0 control bits") {
    val in, out = new Wire
    demux(in, Nil, List(out))

    in.setSignal(false)
    run
    assert(out.getSignal == false, "demux0 - 0")

    in.setSignal(true)
    run
    assert(out.getSignal == true, "demux0 - 1")
  }

  test("demux - 1 control bit") {
    val in, c, out0, out1 = new Wire
    demux(in, List(c), out1 :: List(out0))

    in.setSignal(false)
    c.setSignal(false)
    run
    assert(out0.getSignal == false, "demux1 o0 - 0-0")
    assert(out1.getSignal == false, "demux1 o1 - 0-0")

    in.setSignal(true)
    run
    assert(out0.getSignal == true, "demux1 o0 - 1-0")
    assert(out1.getSignal == false, "demux1 o1 - 1-0")

    c.setSignal(true)
    run
    assert(out0.getSignal == false, "demux1 o0 - 1-1")
    assert(out1.getSignal == true, "demux1 o1 - 1-1")
  }

  test("demux - 2 control bits") {
    val in, c1, c0, o3, o2, o1, o0 = new Wire
    demux(in, List(c1, c0), List(o3, o2, o1, o0))

    in.setSignal(false)
    c1.setSignal(false)
    c0.setSignal(false)
    run
    assert(o0.getSignal === false, "demux1 o0 - 0-0-0)")
    assert(o1.getSignal === false, "demux1 o1 - 0-0-0)")
    assert(o2.getSignal === false, "demux1 o2 - 0-0-0)")
    assert(o3.getSignal === false, "demux1 o3 - 0-0-0)")

    in.setSignal(true)
    run
    assert(o0.getSignal === true , "demux1 o0 - 1-0-0)")
    assert(o1.getSignal === false, "demux1 o1 - 1-0-0)")
    assert(o2.getSignal === false, "demux1 o2 - 1-0-0)")
    assert(o3.getSignal === false, "demux1 o3 - 1-0-0)")

    c0.setSignal(true)
    run
    assert(o0.getSignal === false, "demux1 o0 - 1-0-1)")
    assert(o1.getSignal === true , "demux1 o1 - 1-0-1)")
    assert(o2.getSignal === false, "demux1 o2 - 1-0-1)")
    assert(o3.getSignal === false, "demux1 o3 - 1-0-1)")

    in.setSignal(false)
    run
    assert(o0.getSignal === false, "demux1 o0 - 0-0-0)")
    assert(o1.getSignal === false, "demux1 o1 - 0-0-0)")
    assert(o2.getSignal === false, "demux1 o2 - 0-0-0)")
    assert(o3.getSignal === false, "demux1 o3 - 0-0-0)")

    c0.setSignal(true)
    c1.setSignal(true)
    in.setSignal(true)
    run
    assert(o0.getSignal === false, "demux1 o0 - 1-1-1)")
    assert(o1.getSignal === false, "demux1 o1 - 1-1-1)")
    assert(o2.getSignal === false, "demux1 o2 - 1-1-1)")
    assert(o3.getSignal === true , "demux1 o3 - 1-1-1)")
  }

//  test("circuit check") {
//    check(CircuitCheck)
//  }
}
