package simulations

import common._
import com.sun.corba.se.impl.ior.WireObjectKeyTemplate

class Wire {
  private var sigVal = false
  private var actions: List[Simulator#Action] = List()

  def getSignal: Boolean = sigVal
  
  def setSignal(s: Boolean) {
    if (s != sigVal) {
      sigVal = s
      actions.foreach(action => action())
    }
  }

  def addAction(a: Simulator#Action) {
    actions = a :: actions
    a()
  }
}

abstract class CircuitSimulator extends Simulator {
//  val NoOpDelay: Int
  val InverterDelay: Int
  val AndGateDelay: Int
  val OrGateDelay: Int

  def probe(name: String, wire: Wire) {
    wire addAction {
      () => afterDelay(0) {
        println(
          "  " + currentTime + ": " + name + " -> " +  wire.getSignal)
      }
    }
  }

  def noOpGate(input: Wire, output: Wire) {
    def noOpAction() {
      val inSig = input.getSignal
      afterDelay(0/*NoOpDelay*/) { output.setSignal(inSig) }
    }
    input addAction noOpAction
  }

  def inverter(input: Wire, output: Wire) {
    def invertAction() {
      val inputSig = input.getSignal
      afterDelay(InverterDelay) { output.setSignal(!inputSig) }
    }
    input addAction invertAction
  }

  def andGate(a1: Wire, a2: Wire, output: Wire) {
    def andAction() {
      val a1Sig = a1.getSignal
      val a2Sig = a2.getSignal
      afterDelay(AndGateDelay) { output.setSignal(a1Sig & a2Sig) }
    }
    a1 addAction andAction
    a2 addAction andAction
  }

  def orGate(a1: Wire, a2: Wire, output: Wire) {
    def orAction() {
      val a1Sig = a1.getSignal
      val a2Sig = a2.getSignal
      afterDelay(OrGateDelay) { output.setSignal(a1Sig | a2Sig) }
    }
    a1 addAction orAction
    a2 addAction orAction
  }
  
  def orGate2(a1: Wire, a2: Wire, output: Wire) {
    val na1, na2, aout = new Wire
    inverter(a1, na1)
    inverter(a2, na2)
    andGate(na1, na2, aout)
    inverter(aout, output)
  }

  def demuxGate(in: Wire, c: Wire, out1: Wire, out0: Wire) {
    val nc = new Wire
    inverter(c, nc)
    andGate(in, nc, out0)
    andGate(in, c, out1)
  }

  def demux(in: Wire, c: List[Wire], out: List[Wire]): Unit = c match {
    case Nil => noOpGate(in, out.head)
    case c0 :: Nil => demuxGate(in, c0, out(0), out(1))
    case ci :: cs => {
      val o0, o1 = new Wire
      demuxGate(in, ci, o0, o1)
      val pair = out.splitAt(out.length/2)
      demux(o0, c.tail, pair._1)
      demux(o1, c.tail, pair._2)
    }
  }
}

object Circuit extends CircuitSimulator {
  val NoOpDelay = 0
  val InverterDelay = 1
  val AndGateDelay = 3
  val OrGateDelay = 5

  def andGateExample {
    val in1, in2, out = new Wire
    andGate(in1, in2, out)
    probe("in1", in1)
    probe("in2", in2)
    probe("out", out)
    in1.setSignal(false)
    in2.setSignal(false)
    run

    in1.setSignal(true)
    run

    in2.setSignal(true)
    run
  }

  //
  // to complete with orGateExample and demuxExample...
  //
}

object CircuitMain extends App {
  // You can write tests either here, or better in the test class CircuitSuite.
//  Circuit.andGateExample
}
