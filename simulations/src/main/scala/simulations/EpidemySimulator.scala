package simulations

import math.random

class EpidemySimulator extends Simulator {

  def randomBelow(i: Int) = (random * i).toInt

  def randomDaysToMove = 1 + randomBelow(SimConfig.daysToMove)

  protected[simulations] object SimConfig {
    val population: Int = 300
    val roomRows: Int = 8
    val roomColumns: Int = 8
    val prevalenceRate: Double = 0.01
    val transmissibilityRate: Double = 0.4
    val mortalityRate: Double = 0.25
    val daysToMove: Int = 5
    val daysToSick: Int = 6
    val daysToDie: Int = 14
    val daysToImmune: Int = 16
    val daysToHealthy: Int = 18
    val peopleTakePlanes: Boolean = false
    val takePlaneRate: Double = 0.01
  }

  import SimConfig._

  val persons: List[Person] = for {
    i <- (0 until population).toList
  } yield {
    val person = new Person(i)
    if (i >= (1 - prevalenceRate) * population) person.becomeInfected() else  afterDelay(randomDaysToMove)(person.move)
    person
  }

  class Person(val id: Int) {
    var infected = false
    var sick = false
    var immune = false
    var dead = false

    // demonstrates random number generation
    var row: Int = randomBelow(roomRows)
    var col: Int = randomBelow(roomColumns)

    def looksInfected = sick || dead

    def canGetInfected = !(infected || sick || immune || dead) &&
      (random < transmissibilityRate) &&
      persons.exists(p => (p.row == row) && (p.col == col) && p.infected)

    def becomeInfected() {
      infected = true
      afterDelay(daysToSick)(becomeSick)
      if (random < mortalityRate)
        afterDelay(daysToDie)(die)
      else {
        afterDelay(daysToImmune)(becomeImmune)
        afterDelay(daysToHealthy)(becomeHealthy)
      }
    }

    def becomeSick() {
      sick = infected
    }

    def die() {
      dead = infected && sick
    }

    def becomeImmune() {
      if (infected && sick) {
        immune = true
        sick = false
      }
    }

    def becomeHealthy() {
      if (infected && !sick && immune) {
        infected = false
        sick = false
        immune = false
      }
    }

    def move() {
      val contiguousRooms = List(
        ((row + SimConfig.roomRows - 1) % SimConfig.roomRows, col),
        ((row + 1) % SimConfig.roomRows, col),
        (row, (col + SimConfig.roomColumns - 1) % SimConfig.roomColumns),
        (row, (col + 1) % SimConfig.roomColumns))

      def healthyRoomsToMove(rooms: List[(Int, Int)]): List[(Int, Int)] = {
        for {
          (toRow, toCol) <- rooms if (persons.filter(p => (p.row == toRow) && (p.col == toCol)).forall(p => !p.looksInfected))
        } yield (toRow, toCol)
      }

      if (!dead) {
        if (peopleTakePlanes && random < takePlaneRate) {
          row = randomBelow(roomRows)
          col = randomBelow(roomColumns)
          if (canGetInfected) becomeInfected
        } else {
          val candidateRooms = healthyRoomsToMove(contiguousRooms)
          if (candidateRooms.size != 0) {
            val room =  candidateRooms(randomBelow(candidateRooms.size))
            row = room._1
            col = room._2
            if (canGetInfected) becomeInfected
          }
        }

        afterDelay(randomDaysToMove)(move)
      }
    }

    def XO(b: Boolean) = {
      if (b) "X" else "O"
    }

    override def toString = f"Person [id=$id%3d row=$row col=$col inf=${XO(infected)} sic=${XO(sick)} imm=${XO(immune)} dea=${XO(dead)}]"
  }

}
