package nodescala


import scala.language.postfixOps
import scala.util.{Try, Success, Failure}
import scala.collection._
import scala.concurrent._
import ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.async.Async.{async, await}
import org.scalatest._
import NodeScala._
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import java.util.NoSuchElementException
import scala.NoSuchElementException

@RunWith(classOf[JUnitRunner])
class NodeScalaSuite extends FunSuite {

  test("A Future should always be created") {
    val always = Future.always(517)

    assert(Await.result(always, 0 nanos) == 517)
  }

  test("A Future should never be created") {
    val never = Future.never[Int]

    try {
      Await.result(never, 1 second)
      assert(false)
    } catch {
      case t: TimeoutException => // ok!
    }
  }

  test("all should return a Future of a list of future results in order") {
    val all = Future.all(List(Future.always(1), Future.always(2)))
    val rs = Await.result(all, 1 seconds)

    assert(rs.length == 2)
    assert(rs.head == 1)
    assert(rs.tail.head == 2)
  }

  test("all should fail if any Future fails") {
    val all = Future.all(List(Future.always(1), Future.failed(new NoSuchElementException)))

    try {
      Await.result(all, 1 seconds)
      assert(false)
    } catch {
      case t: NoSuchElementException => // all good!
    }
  }

  test("any should return the result of any Future in the list, success or failure") {
    val any = Future.any(List(Future.always({
      Thread.sleep(500); 1
    }), Future.failed(new Exception)))

    try {
      assert(Await.result(any, 1 seconds) == 1)
    } catch {
      case t: NoSuchElementException => assert(false, "got NoSuchElementException")
      case t: Exception => // all good
    }
  }

  test("delay should wait for a certain time") {
    val start = System.nanoTime()
    val delay = Future.delay(500 milliseconds)

    Await.result(delay, 1 seconds)
    val stop = System.nanoTime()
    assert((stop - start) > 500000)
  }

  test("now returns the value of a quick Future") {
    val f = Future.always(1)

    assert(f.now == 1)
  }

  test("now throws NoSuchElement exception on a Future that is not ready") {
    val f = Future.delay(500 milliseconds)

    try {
      f.now
      assert(false)
    } catch {
      case t: NoSuchElementException => // all good
    }
  }

  test("continueWith maps the first Future onto a second Future") {
    val f = Future.always(5)
    val cont = f.continueWith(fi => "The value of the Future is: " + fi.value.get.get)

    Await.result(cont, 500 milliseconds)
    assert(cont.now.contentEquals("The value of the Future is: 5"))
  }

  test("CancellationTokenSource should allow stopping the computation") {
    val cts = CancellationTokenSource()
    val ct = cts.cancellationToken
    val p = Promise[String]()

    async {
      while (ct.nonCancelled) {
        // do work
      }

      p.success("done")
    }

    cts.unsubscribe()
    assert(Await.result(p.future, 1 second) == "done")
  }

  class DummyExchange(val request: Request) extends Exchange {
    @volatile var response = ""
    val loaded = Promise[String]()

    def write(s: String) {
      response += s
    }

    def close() {
      loaded.success(response)
    }
  }

  class DummyListener(val port: Int, val relativePath: String) extends NodeScala.Listener {
    self =>

    @volatile private var started = false
    var handler: Exchange => Unit = null

    def createContext(h: Exchange => Unit) = this.synchronized {
      assert(started, "is server started?")
      handler = h
    }

    def removeContext() = this.synchronized {
      assert(started, "is server started?")
      handler = null
    }

    def start() = self.synchronized {
      started = true
      new Subscription {
        def unsubscribe() = self.synchronized {
          started = false
        }
      }
    }

    def emit(req: Request) = {
      val exchange = new DummyExchange(req)
      if (handler != null) handler(exchange)
      exchange
    }
  }

  class DummyServer(val port: Int) extends NodeScala {
    self =>
    val listeners = mutable.Map[String, DummyListener]()

    def createListener(relativePath: String) = {
      val l = new DummyListener(port, relativePath)
      listeners(relativePath) = l
      l
    }

    def emit(relativePath: String, req: Request) = this.synchronized {
      val l = listeners(relativePath)
      l.emit(req)
    }
  }

  test("Listener should serve the next request as a future") {
    val dummy = new DummyListener(8191, "/test")
    val subscription = dummy.start()

    def test(req: Request) {
      val f = dummy.nextRequest()
      dummy.emit(req)
      val (reqReturned, xchg) = Await.result(f, 1 second)

      assert(reqReturned == req)
    }

    test(immutable.Map("StrangeHeader" -> List("StrangeValue1")))
    test(immutable.Map("StrangeHeader" -> List("StrangeValue2")))

    subscription.unsubscribe()
  }

  test("Server should serve requests") {
    val dummy = new DummyServer(8191)
    val dummySubscription = dummy.start("/testDir") {
      request => for (kv <- request.iterator) yield (kv + "\n").toString
    }

    // wait until server is really installed
    Thread.sleep(500)

    def test(req: Request) {
      val webpage = dummy.emit("/testDir", req)
      val content = Await.result(webpage.loaded.future, 1 second)
      val expected = (for (kv <- req.iterator) yield (kv + "\n").toString).mkString
      assert(content == expected, s"'$content' vs. '$expected'")
    }

    test(immutable.Map("StrangeRequest" -> List("Does it work?")))
    test(immutable.Map("StrangeRequest" -> List("It works!")))
    test(immutable.Map("WorksForThree" -> List("Always works. Trust me.")))

    dummySubscription.unsubscribe()
  }

}




